﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachineKataWFA;
using VendingMachineKataWFA.Enums;

namespace VendingMachineKataWFATests
{
    [TestClass]
    public class ProductHelperTests
    {
        [TestMethod]
        public void ColaCostADollar()
        {
            Assert.AreEqual(1m, new ProductHelper().GetProductCost(Product.Cola));
        }

        [TestMethod]
        public void ChipsCostFiftyCents()
        {
            Assert.AreEqual(0.5m, new ProductHelper().GetProductCost(Product.Chips));
        }

        [TestMethod]
        public void CandyCostSixtyFiveCents()
        {
            Assert.AreEqual(0.65m, new ProductHelper().GetProductCost(Product.Candy));
        }

        [TestMethod]
        public void GetAvailableProducts()
        {
            var availableProducts = new ProductHelper().GetAvailableProducts();

            Assert.IsTrue(availableProducts != null, "Available products not loaded.");
        }
    }
}
