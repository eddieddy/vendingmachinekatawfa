﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachineKataWFA;
using VendingMachineKataWFA.Enums;

namespace VendingMachineKataWFATests
{
    [TestClass]
    public class VendingMachineEngineTests
    {
        private VendingMachineEngine vendingMachineEngine = new VendingMachineEngine();
        private void EnsureAvailableCoins(int count)
        {
            vendingMachineEngine.AvailableCoins.Clear();
            vendingMachineEngine.AvailableCoins = new CoinHelper().GetAvailableCoins(count);
        }

        [TestMethod]
        public void DisplayPriceOfColaIfZeroBalance()
        {
            vendingMachineEngine.PurchaseProduct(Product.Cola);
            Assert.AreEqual("PRICE: " + "$1.00", vendingMachineEngine.GetDisplayText());
        }

        [TestMethod]
        public void DisplayThankYouWhenColaPurchased()
        {
            vendingMachineEngine.InsertCoin(Coin.Quarter);
            vendingMachineEngine.InsertCoin(Coin.Quarter);
            vendingMachineEngine.InsertCoin(Coin.Quarter);
            vendingMachineEngine.InsertCoin(Coin.Quarter);

            vendingMachineEngine.PurchaseProduct(Product.Cola);

            Assert.AreEqual("THANK YOU", vendingMachineEngine.GetDisplayText());
        }

        [TestMethod]
        public void DisplayBalanceAfterColaTryWithoutEnoughBalanceThenDimeInserted()
        {
            vendingMachineEngine.PurchaseProduct(Product.Cola);

            vendingMachineEngine.GetDisplayText();

            vendingMachineEngine.InsertCoin(Coin.Dime);

            Assert.AreEqual("$0.10", vendingMachineEngine.GetDisplayText());
        }

        [TestMethod]
        public void DisplayInsertCoinAfterThankYouWhenColaPurchasedAndThankyouDisplayedOnce()
        {
            vendingMachineEngine.InsertCoin(Coin.Quarter);
            vendingMachineEngine.InsertCoin(Coin.Quarter);
            vendingMachineEngine.InsertCoin(Coin.Quarter);
            vendingMachineEngine.InsertCoin(Coin.Quarter);

            vendingMachineEngine.PurchaseProduct(Product.Cola);

            Assert.AreEqual("THANK YOU", vendingMachineEngine.GetDisplayText());
            EnsureAvailableCoins(4);
            Assert.AreEqual("INSERT COIN", vendingMachineEngine.GetDisplayText());
        }

        [TestMethod]
        public void InsertInvalidCoinAddToRejected()
        {
            Assert.IsTrue(vendingMachineEngine.ReturnedCoins.Count == 0);

            vendingMachineEngine.InsertCoin(Coin.Penny);

            Assert.IsTrue(vendingMachineEngine.ReturnedCoins.Count == 1);
        }

        [TestMethod]
        public void InsertQuarterAddsTwentyFiveCentsToBalance()
        {
            vendingMachineEngine.InsertCoin(Coin.Quarter);
            Assert.AreEqual("$0.25", vendingMachineEngine.GetDisplayText());
        }

        [TestMethod]
        public void InsertDimeAddsTenToBalance()
        {
            vendingMachineEngine.InsertCoin(Coin.Dime);
            Assert.AreEqual("$0.10", vendingMachineEngine.GetDisplayText());
        }

        [TestMethod]
        public void ZeroBalanceDisplaysInsertCoin()
        {
            EnsureAvailableCoins(4);
            Assert.AreEqual("INSERT COIN", vendingMachineEngine.GetDisplayText());
        }

        [TestMethod]
        public void PurchaseChipsWithSixtyCentsGiveTenCentsChange()
        {
            vendingMachineEngine.InsertCoin(Coin.Quarter);
            vendingMachineEngine.InsertCoin(Coin.Quarter);
            vendingMachineEngine.InsertCoin(Coin.Dime);

            EnsureAvailableCoins(4);

            vendingMachineEngine.PurchaseProduct(Product.Chips);

            Assert.IsTrue(vendingMachineEngine.ReturnedCoins.Any(), "No coins returned when more than one is expected.");
            var changeAmount = vendingMachineEngine.ReturnedCoins.Select(s => new CoinHelper().GetCoinValue(s)).Sum();

            Assert.AreEqual(0.10m, changeAmount);
        }

        [TestMethod]
        public void PurchaseCandyWithADollarGivesThirtyFiveCentsChange()
        {
            vendingMachineEngine.InsertCoin(Coin.Quarter);
            vendingMachineEngine.InsertCoin(Coin.Quarter);
            vendingMachineEngine.InsertCoin(Coin.Quarter);
            vendingMachineEngine.InsertCoin(Coin.Quarter);

            EnsureAvailableCoins(4);

            vendingMachineEngine.PurchaseProduct(Product.Candy);

            Assert.IsTrue(vendingMachineEngine.ReturnedCoins.Any(), "No coins returned when more than one is expected.");
            var changeAmount = vendingMachineEngine.ReturnedCoins.Select(s => new CoinHelper().GetCoinValue(s)).Sum();

            Assert.AreEqual(0.35m, changeAmount);
        }

        [TestMethod]
        public void ReturnQuarterInCoins()
        {
            vendingMachineEngine.InsertCoin(Coin.Quarter);

            vendingMachineEngine.ReturnCoinBalance();
            Assert.IsTrue(vendingMachineEngine.ReturnedCoins.Count > 0, "No coins returned when more than one is expected.");

            var changeAmount = vendingMachineEngine.ReturnedCoins.Select(s => new CoinHelper().GetCoinValue(s)).Sum();

            Assert.AreEqual(0.25m, changeAmount);
        }

        [TestMethod]
        public void ReturnADollarInCoins()
        {
            vendingMachineEngine.InsertCoin(Coin.Quarter);
            vendingMachineEngine.InsertCoin(Coin.Quarter);
            vendingMachineEngine.InsertCoin(Coin.Quarter);
            vendingMachineEngine.InsertCoin(Coin.Quarter);

            vendingMachineEngine.ReturnCoinBalance();
            Assert.IsTrue(vendingMachineEngine.ReturnedCoins.Count > 0, "No coins returned when more than one is expected.");

            var changeAmount = vendingMachineEngine.ReturnedCoins.Select(s => new CoinHelper().GetCoinValue(s)).Sum();

            Assert.AreEqual(1.0m, changeAmount);
        }

        [TestMethod]
        public void WhenCoinsReturnedInserCoinDisplays()
        {
            vendingMachineEngine.InsertCoin(Coin.Quarter);
            vendingMachineEngine.ReturnCoinBalance();
            EnsureAvailableCoins(4);
            Assert.AreEqual("INSERT COIN", vendingMachineEngine.GetDisplayText());
        }

        [TestMethod]
        public void DisplaySoldOutWhenProductIsNotAvailable()
        {
            vendingMachineEngine.AvailableProducts.Clear();

            vendingMachineEngine.PurchaseProduct(Product.Candy);
            EnsureAvailableCoins(4);
            Assert.AreEqual("SOLD OUT", vendingMachineEngine.GetDisplayText());
        }

        [TestMethod]
        public void DisplayInsertCoinAfterSoldOutWhenProductIsNotAvailable()
        {
            vendingMachineEngine.AvailableProducts.Clear();

            vendingMachineEngine.PurchaseProduct(Product.Candy);
            EnsureAvailableCoins(4);

            vendingMachineEngine.GetDisplayText();
            
            Assert.AreEqual("INSERT COIN", vendingMachineEngine.GetDisplayText());
        }

        [TestMethod]
        public void DisplayBalanceAfterSoldOutWhenProductIsNotAvailable()
        {
            vendingMachineEngine.AvailableProducts.Clear();

            vendingMachineEngine.InsertCoin(Coin.Dime);

            vendingMachineEngine.PurchaseProduct(Product.Candy);
            EnsureAvailableCoins(1);
            vendingMachineEngine.GetDisplayText();
            Assert.AreEqual("$0.10", vendingMachineEngine.GetDisplayText());
        }

        [TestMethod]
        public void ProductRemovedFromAvailableProductsWhenSold()
        {
            vendingMachineEngine.AvailableProducts.Clear();
            vendingMachineEngine.AvailableProducts.Add(Product.Candy);

            vendingMachineEngine.InsertCoin(Coin.Quarter);
            vendingMachineEngine.InsertCoin(Coin.Quarter);
            vendingMachineEngine.InsertCoin(Coin.Quarter);
            vendingMachineEngine.PurchaseProduct(Product.Candy);
            vendingMachineEngine.GetDisplayText();

            Assert.IsTrue(vendingMachineEngine.AvailableProducts.Count == 0, "Product was not removed from available products.");
        }

        [TestMethod]
        public void DisplayExactChangeInstedOfInsertCoinIfRequired()
        {
            vendingMachineEngine.AvailableCoins.Clear();

            Assert.AreEqual("EXACT CHANGE ONLY", vendingMachineEngine.GetDisplayText());
        }
    }
}
