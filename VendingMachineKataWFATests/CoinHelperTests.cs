﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VendingMachineKataWFA;
using VendingMachineKataWFA.Enums;

namespace VendingMachineKataWFATests
{
    [TestClass]
    public class CoinHelperTests
    {
        private CoinHelper coinHelper = new CoinHelper();

        private List<Coin> AvailableCoins(int count)
        {
            return coinHelper.GetAvailableCoins(count);
        }

        [TestMethod]
        public void QuarterIsValidCoin()
        {
            Assert.IsTrue(coinHelper.IsValidCoin(Coin.Quarter));
        }

        [TestMethod]
        public void DimeIsValidCoin()
        {
            Assert.IsTrue(coinHelper.IsValidCoin(Coin.Dime));
        }

        [TestMethod]
        public void NickelIsValidCoin()
        {
            Assert.IsTrue(coinHelper.IsValidCoin(Coin.Nickel));
        }

        [TestMethod]
        public void PennyIsNotValidCoin()
        {
            Assert.IsFalse(coinHelper.IsValidCoin(Coin.Penny));
        }

        [TestMethod]
        public void QuarterIsTwentyFiveCents()
        {
            Assert.AreEqual(CoinHelper.QuarterValue, coinHelper.GetCoinValue(Coin.Quarter));
        }

        [TestMethod]
        public void DimeIsTenCents()
        {
            Assert.AreEqual(CoinHelper.DimeValue, coinHelper.GetCoinValue(Coin.Dime));
        }

        [TestMethod]
        public void NickelIsFiveCents()
        {
            Assert.AreEqual(CoinHelper.NickelValue, coinHelper.GetCoinValue(Coin.Nickel));
        }

        [TestMethod]
        public void PennyIsZeroCents()
        {
            Assert.AreEqual(0.0m, coinHelper.GetCoinValue(Coin.Penny));
        }

        [TestMethod]
        public void MakeChangeZeroChangeforZeroBalance()
        {
            var change = coinHelper.MakeChange(0.0m, AvailableCoins(1));

            Assert.IsTrue(change.Count == 0);
        }

        [TestMethod]
        public void MakeChangeforFiveCents()
        {
            var change = coinHelper.MakeChange(0.05m, AvailableCoins(1));
            
            Assert.IsTrue(change.Count == 1);
            Assert.AreEqual(Coin.Nickel, change.FirstOrDefault());
        }

        [TestMethod]
        public void MakeChangeforTenCents()
        {
            var change = coinHelper.MakeChange(0.10m, AvailableCoins(1));

            Assert.IsTrue(change.Count == 1);
            Assert.AreEqual(Coin.Dime, change.FirstOrDefault());
        }

        [TestMethod]
        public void MakeChangeforTwentyFiveCents()
        {
            var change = coinHelper.MakeChange(0.25m, AvailableCoins(1));

            Assert.IsTrue(change.Count == 1);
            Assert.AreEqual(Coin.Quarter, change.FirstOrDefault());
        }

        [TestMethod]
        public void MakeChangeforThirtyFiveCents()
        {
            var change = coinHelper.MakeChange(0.35m, AvailableCoins(1));

            Assert.IsTrue(change.Count > 1);
            var changeAmount = change.Select(s => coinHelper.GetCoinValue(s)).Sum();
            Assert.AreEqual(0.35m, changeAmount);
        }

        [TestMethod]
        public void MakeChangeforFiftyFiveCents()
        {
            var change = coinHelper.MakeChange(0.55m, AvailableCoins(4));

            Assert.IsTrue(change.Count > 1);
            var changeAmount = change.Select(s => coinHelper.GetCoinValue(s)).Sum();
            Assert.AreEqual(0.55m, changeAmount);
        }

        [TestMethod]
        public void TestGetAvailableCoins()
        {
            var availableCoins = AvailableCoins(1);

            Assert.IsTrue(availableCoins != null, "Availble coins was not expected to be null.");
        }

        [TestMethod]
        public void TestExactChangeForNoAvailableCoins()
        {
            Assert.AreEqual(true, coinHelper.ExactChange(new List<Coin>()));
        }

        [TestMethod]
        public void TestExactChangeRequiredWithTwoQuartersAvailable()
        {
            var availableCoins = new[] { Coin.Quarter, Coin.Quarter }.ToList();

            Assert.AreEqual(true, coinHelper.ExactChange(availableCoins));
        }

        [TestMethod]
        public void TestExactChangeForWithOneQuarterAvailable()
        {
            var availableCoins = new[] { Coin.Quarter }.ToList();

            Assert.AreEqual(true, coinHelper.ExactChange(availableCoins));
        }

        [TestMethod]
        public void TestExactChangeRequiredWithFiveDimesAvailable()
        {
            var availableCoins = new[] { Coin.Dime, Coin.Dime, Coin.Dime, Coin.Dime, Coin.Dime }.ToList();

            Assert.AreEqual(true, coinHelper.ExactChange(availableCoins));
        }

        [TestMethod]
        public void TestExactChangeRequiredWithFourQuartersAvailable()
        {
            var availableCoins = new[] { Coin.Quarter, Coin.Quarter, Coin.Quarter, Coin.Quarter }.ToList();

            Assert.AreEqual(true, coinHelper.ExactChange(availableCoins));
        }

        [TestMethod]
        public void TestExactChangeNotRequiredWithTwoQuartersFourDimesAndTwoNickelsAvailable()
        {
            var availableCoins = new[] { Coin.Quarter, Coin.Quarter, Coin.Dime, Coin.Dime, Coin.Dime, Coin.Dime, Coin.Nickel, Coin.Nickel }.ToList();

            Assert.AreEqual(false, coinHelper.ExactChange(availableCoins));
        }

        [TestMethod]
        public void AvailableCoinsReducedByChangeReturned()
        {
            var availableCoins = AvailableCoins(1);

            Assert.AreEqual(4, availableCoins.Count(), "Expect four coins available.");
            coinHelper.MakeChange(coinHelper.GetCoinValue(Coin.Quarter), availableCoins);
            Assert.AreEqual(3, availableCoins.Count(), "Expect three coins available.");
            Assert.IsTrue(availableCoins.Any(ac => ac == Coin.Quarter) == false, "Quarter not expected to be available.");
        }
    }
}
