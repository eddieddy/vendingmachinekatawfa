# README #

This README document explains steps are necessary to get your application up and running.

### This is a project that mimics a vending machine. ###

* This project is a C# Windows Forms Application that allows you to purchase vending machine pop, chips, and candy.
  The project was created with Visual Studio 2015 Professional.
* Version 1.0

### How do I get set up? ###

* Download source and open with Visual Studio.
* No configuration necessary.
* No dependencies necessary.

* To run tests with Visual Studio. Open solution with Visual Studio, select Test => Run => All tests.
* To run tests from command line:
* 1. Download source file to you local machine.
  2. Open Developer Command Prompt for Visual Studio
  3. Navigate the command prompt window to the folder for the project VendingMachineKataWFA with the VendingMachineKataWFA.csproj file in it.
  4. Type in the command prompt 'devenv VendingMachineKataWFA.csproj /build'
  5. Navigate the command prompt window to the folder that the VendingMachineKataWFATests.csproj file is located.
  6. Type in the command prompt 'devenv VendingMachineKataWFATests.csproj /build'
  7. Navigate the command prompt window to the bin => debug folder for VendingMachineKataWFATest, which should now contain the 
     VendingMachineKataWFATests.dll file.
  8. Type in the command prompt 'vstest.console.exe VendingMachineKataWFATests.dll'

### How do run it? ###

* To run the application with Visual Studio, open the solution and press Debug => Start Debugging on the tool bar, or press F5.
* If the project was build with the Developer Command Prompt for Visual Studio, then you can navigate to the folder 
   VendingMachineKataWFA\VendingMachineKataWFA\bin\Debug and double click the Application, VendingMachineKataWFA.exe, file.