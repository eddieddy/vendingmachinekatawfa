﻿namespace VendingMachineKataWFA.Enums
{
    public enum Coin
    {
        Quarter,
        Dime,
        Nickel,
        Penny
    }
}
