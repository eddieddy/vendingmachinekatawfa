﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VendingMachineKataWFA.Enums;

namespace VendingMachineKataWFA
{
    public partial class Form1 : Form
    {
        private VendingMachineEngine vendingMachineEngine;

        public Form1()
        {
            InitializeComponent();

            cboAvailableCoins.DataSource = Enum.GetValues(typeof(Coin));
            vendingMachineEngine = new VendingMachineEngine();
            UpdateInterface();
        }

        public void UpdateInterface()
        {
            lblDisplay.Text = vendingMachineEngine.GetDisplayText();

            lsvReturnedCoins.Items.Clear();

            lsvReturnedCoins.Items.AddRange(vendingMachineEngine.ReturnedCoins.Select(s => new ListViewItem { Text = s.ToString()}).ToArray());
        }

        private void btnInsertCoin_Click(object sender, EventArgs e)
        {
            vendingMachineEngine.InsertCoin((Coin)cboAvailableCoins.SelectedItem);

            UpdateInterface();
        }

        private void btnPickupRejectedCoins_Click(object sender, EventArgs e)
        {
            vendingMachineEngine.ReturnedCoins.Clear();
            UpdateInterface();
        }

        private void btnCola_Click(object sender, EventArgs e)
        {
            vendingMachineEngine.PurchaseProduct(Product.Cola);
            UpdateInterface();
        }

        private void btnChips_Click(object sender, EventArgs e)
        {
            vendingMachineEngine.PurchaseProduct(Product.Chips);
            UpdateInterface();
        }

        private void btnCandy_Click(object sender, EventArgs e)
        {
            vendingMachineEngine.PurchaseProduct(Product.Candy);
            UpdateInterface();
        }

        private void btnReturnCoinBalance_Click(object sender, EventArgs e)
        {
            vendingMachineEngine.ReturnCoinBalance();
            UpdateInterface();
        }
    }
}
