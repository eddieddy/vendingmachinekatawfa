﻿using System;
using System.Collections.Generic;
using System.Linq;
using VendingMachineKataWFA.Enums;

namespace VendingMachineKataWFA
{
    public class CoinHelper
    {
        public const decimal QuarterValue = 0.25m;
        public const decimal DimeValue = 0.10m;
        public const decimal NickelValue = 0.05m;

        public bool IsValidCoin(Coin coin)
        {
            return coin == Coin.Quarter || coin == Coin.Dime || coin == Coin.Nickel;
        }

        public decimal GetCoinValue(Coin coin)
        {
            switch (coin)
            {
                case Coin.Quarter:
                    return QuarterValue;
                case Coin.Dime:
                    return DimeValue;
                case Coin.Nickel:
                    return NickelValue;
                default:
                    return 0;
            }
        }

        public List<Coin> MakeChange(decimal change, List<Coin> availableCoins)
        {
            var returnChange = new List<Coin>();

            do
            {
                // All work done in checks.
            } while (AddCoinToReturn(Coin.Quarter, ref change, availableCoins, returnChange)
                     || AddCoinToReturn(Coin.Dime, ref change, availableCoins, returnChange)
                     || AddCoinToReturn(Coin.Nickel, ref change, availableCoins, returnChange));

            return returnChange;
        }

        public bool AddCoinToReturn(Coin coin, ref decimal balance, List<Coin> availableCoins, List<Coin> returnCoins)
        {
            var hasCoin = GetCoinValue(coin) <= balance && availableCoins.Count(ac => ac == coin) > 0;

            if (hasCoin)
            {
                returnCoins.Add(coin);
                availableCoins.Remove(coin);
                balance -= GetCoinValue(coin);
            }

            return hasCoin;
        }

        public List<Coin> GetAvailableCoins(int? count = null)
        {
            var returnCoins = new List<Coin>();
            var random = new Random(DateTime.Now.Millisecond);

            foreach (var p in Enum.GetValues(typeof(Coin)))
            {
                var minRandom = 0;
                var maxRandom = 20;
                count = count ?? random.Next(minRandom, maxRandom);
                for (var i = 0; i < count; i++)
                {
                    returnCoins.Add((Coin)p);
                }
            }

            return returnCoins;
        }

        public bool ExactChange(List<Coin> availableCoins)
        {
            if (!availableCoins.Any())
            {
                return true;
            }

            foreach (Product p in Enum.GetValues(typeof(Product)))
            {
                var localAvailableCoins = availableCoins.ToList();
                var change = MakeChange(new ProductHelper().GetProductCost(p), localAvailableCoins);
                if (change.Sum(s => GetCoinValue(s)) < new ProductHelper().GetProductCost(p))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
