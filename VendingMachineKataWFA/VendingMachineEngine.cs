﻿using System.Collections.Generic;
using System.Linq;
using VendingMachineKataWFA.Enums;

namespace VendingMachineKataWFA
{
    public class VendingMachineEngine
    {
        public List<Coin> ReturnedCoins;
        public List<Coin> InsertedCoins;
        private decimal _balance;
        private Product? _selectedProduct;
        private bool _purchaseSuccess;

        public List<Coin> AvailableCoins;

        public List<Product> AvailableProducts { get; set; }
        private Product? SoldAvailableProduct { get; set; }

        public VendingMachineEngine()
        {
            ReturnedCoins = new List<Coin>();
            InsertedCoins = new List<Coin>();
            _balance = 0m;
            _selectedProduct = null;
            _purchaseSuccess = false;
            AvailableProducts = new ProductHelper().GetAvailableProducts();
            SoldAvailableProduct = null;
            AvailableCoins = new CoinHelper().GetAvailableCoins();
        }

        public void InsertCoin(Coin coin)
        {
            if (new CoinHelper().IsValidCoin(coin) == false)
            {
                ReturnedCoins.Add(coin);
            }
            else
            {
                InsertedCoins.Add(coin);
                _balance += new CoinHelper().GetCoinValue(coin);
            }
        }

        public string GetDisplayText()
        {
            if (_selectedProduct.HasValue)
            {
                if (AvailableProducts.Contains(_selectedProduct.Value) == false)
                {
                    _selectedProduct = null;
                    return "SOLD OUT";
                }
                if (_purchaseSuccess == false)
                {
                    var returnValue = "PRICE: " + new ProductHelper().GetProductCost(_selectedProduct.Value).ToString("c");
                    _selectedProduct = null;
                    return returnValue;
                }

                if (SoldAvailableProduct.HasValue)
                {
                    AvailableProducts.Remove(SoldAvailableProduct.Value);
                    SoldAvailableProduct = null;
                }

                _purchaseSuccess = false;
                _selectedProduct = null;
                return "THANK YOU";
            }

            if (_balance == 0)
            {
                if (new CoinHelper().ExactChange(AvailableCoins))
                {
                    return "EXACT CHANGE ONLY";
                }

                return "INSERT COIN";
            }

            return _balance.ToString("c");

        }

        public void PurchaseProduct(Product product)
        {
            _selectedProduct = product;

            _purchaseSuccess = AvailableProducts.Contains(product) && _balance >= new ProductHelper().GetProductCost(product);

            if (_purchaseSuccess)
            {
                _balance -= new ProductHelper().GetProductCost(product);
                if (new CoinHelper().ExactChange(AvailableCoins) == false)
                {
                    ReturnedCoins.AddRange(new CoinHelper().MakeChange(_balance, AvailableCoins));
                }
                SoldAvailableProduct = AvailableProducts.FirstOrDefault(ap => ap == _selectedProduct.Value);
                _balance = 0.0m;
            }
        }

        public void ReturnCoinBalance()
        {
            ReturnedCoins.AddRange(InsertedCoins);
            _balance = 0m;
        }
    }
}
