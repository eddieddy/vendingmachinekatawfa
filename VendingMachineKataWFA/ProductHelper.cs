﻿using System;
using System.Collections.Generic;
using VendingMachineKataWFA.Enums;

namespace VendingMachineKataWFA
{
    public class ProductHelper
    {
        public decimal GetProductCost(Product product)
        {
            if (product == Product.Cola)
                return 1m;

            if (product == Product.Chips)
                return 0.5m;

            if (product == Product.Candy)
                return 0.65m;

            throw new NotImplementedException("Cost for product is not defined.");
        }

        public List<Product> GetAvailableProducts()
        {
            var returnProducts = new List<Product>();
            var random = new Random(DateTime.Now.Millisecond);

            foreach (var p in Enum.GetValues(typeof(Product)))
            {
                var minRandom = 0;
                var maxRandom = 2;
                var count = random.Next(minRandom , maxRandom);
                for (var i = 0; i <= count; i++)
                {
                    returnProducts.Add((Product)p);
                }
            }

            return returnProducts;
        }
    }
}
