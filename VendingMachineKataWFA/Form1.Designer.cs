﻿namespace VendingMachineKataWFA
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnReturnCoinBalance = new System.Windows.Forms.Button();
            this.btnCandy = new System.Windows.Forms.Button();
            this.btnChips = new System.Windows.Forms.Button();
            this.btnCola = new System.Windows.Forms.Button();
            this.btnInsertCoin = new System.Windows.Forms.Button();
            this.cboAvailableCoins = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblDisplay = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPickupRejectedCoins = new System.Windows.Forms.Button();
            this.lsvReturnedCoins = new System.Windows.Forms.ListView();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnReturnCoinBalance);
            this.groupBox1.Controls.Add(this.btnCandy);
            this.groupBox1.Controls.Add(this.btnChips);
            this.groupBox1.Controls.Add(this.btnCola);
            this.groupBox1.Controls.Add(this.btnInsertCoin);
            this.groupBox1.Controls.Add(this.cboAvailableCoins);
            this.groupBox1.Location = new System.Drawing.Point(263, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(338, 230);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "User Input";
            // 
            // btnReturnCoinBalance
            // 
            this.btnReturnCoinBalance.Location = new System.Drawing.Point(250, 21);
            this.btnReturnCoinBalance.Name = "btnReturnCoinBalance";
            this.btnReturnCoinBalance.Size = new System.Drawing.Size(75, 66);
            this.btnReturnCoinBalance.TabIndex = 6;
            this.btnReturnCoinBalance.Text = "Return Coins";
            this.btnReturnCoinBalance.UseVisualStyleBackColor = true;
            this.btnReturnCoinBalance.Click += new System.EventHandler(this.btnReturnCoinBalance_Click);
            // 
            // btnCandy
            // 
            this.btnCandy.Location = new System.Drawing.Point(250, 149);
            this.btnCandy.Name = "btnCandy";
            this.btnCandy.Size = new System.Drawing.Size(75, 62);
            this.btnCandy.TabIndex = 5;
            this.btnCandy.Text = "Candy $0.65";
            this.btnCandy.UseVisualStyleBackColor = true;
            this.btnCandy.Click += new System.EventHandler(this.btnCandy_Click);
            // 
            // btnChips
            // 
            this.btnChips.Location = new System.Drawing.Point(136, 149);
            this.btnChips.Name = "btnChips";
            this.btnChips.Size = new System.Drawing.Size(75, 62);
            this.btnChips.TabIndex = 4;
            this.btnChips.Text = "Chips $0.50";
            this.btnChips.UseVisualStyleBackColor = true;
            this.btnChips.Click += new System.EventHandler(this.btnChips_Click);
            // 
            // btnCola
            // 
            this.btnCola.Location = new System.Drawing.Point(14, 149);
            this.btnCola.Name = "btnCola";
            this.btnCola.Size = new System.Drawing.Size(75, 62);
            this.btnCola.TabIndex = 3;
            this.btnCola.Text = "Cola $1.00";
            this.btnCola.UseVisualStyleBackColor = true;
            this.btnCola.Click += new System.EventHandler(this.btnCola_Click);
            // 
            // btnInsertCoin
            // 
            this.btnInsertCoin.Location = new System.Drawing.Point(136, 21);
            this.btnInsertCoin.Name = "btnInsertCoin";
            this.btnInsertCoin.Size = new System.Drawing.Size(75, 66);
            this.btnInsertCoin.TabIndex = 1;
            this.btnInsertCoin.Text = "Insert Coin";
            this.btnInsertCoin.UseVisualStyleBackColor = true;
            this.btnInsertCoin.Click += new System.EventHandler(this.btnInsertCoin_Click);
            // 
            // cboAvailableCoins
            // 
            this.cboAvailableCoins.FormattingEnabled = true;
            this.cboAvailableCoins.Location = new System.Drawing.Point(7, 31);
            this.cboAvailableCoins.Name = "cboAvailableCoins";
            this.cboAvailableCoins.Size = new System.Drawing.Size(110, 24);
            this.cboAvailableCoins.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblDisplay);
            this.groupBox2.Location = new System.Drawing.Point(24, 25);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(218, 100);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Display";
            // 
            // lblDisplay
            // 
            this.lblDisplay.AutoSize = true;
            this.lblDisplay.Location = new System.Drawing.Point(6, 31);
            this.lblDisplay.Name = "lblDisplay";
            this.lblDisplay.Size = new System.Drawing.Size(95, 17);
            this.lblDisplay.TabIndex = 0;
            this.lblDisplay.Text = "INSERT COIN";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.btnPickupRejectedCoins);
            this.groupBox3.Controls.Add(this.lsvReturnedCoins);
            this.groupBox3.Location = new System.Drawing.Point(334, 276);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(267, 180);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Output";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Coin Return";
            // 
            // btnPickupRejectedCoins
            // 
            this.btnPickupRejectedCoins.Location = new System.Drawing.Point(179, 60);
            this.btnPickupRejectedCoins.Name = "btnPickupRejectedCoins";
            this.btnPickupRejectedCoins.Size = new System.Drawing.Size(75, 97);
            this.btnPickupRejectedCoins.TabIndex = 1;
            this.btnPickupRejectedCoins.Text = "Pick up coins";
            this.btnPickupRejectedCoins.UseVisualStyleBackColor = true;
            this.btnPickupRejectedCoins.Click += new System.EventHandler(this.btnPickupRejectedCoins_Click);
            // 
            // lsvReturnedCoins
            // 
            this.lsvReturnedCoins.Location = new System.Drawing.Point(19, 60);
            this.lsvReturnedCoins.Name = "lsvReturnedCoins";
            this.lsvReturnedCoins.Size = new System.Drawing.Size(121, 97);
            this.lsvReturnedCoins.TabIndex = 0;
            this.lsvReturnedCoins.UseCompatibleStateImageBehavior = false;
            this.lsvReturnedCoins.View = System.Windows.Forms.View.SmallIcon;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(696, 509);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Vending Machine";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cboAvailableCoins;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblDisplay;
        private System.Windows.Forms.Button btnInsertCoin;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPickupRejectedCoins;
        private System.Windows.Forms.ListView lsvReturnedCoins;
        private System.Windows.Forms.Button btnCola;
        private System.Windows.Forms.Button btnCandy;
        private System.Windows.Forms.Button btnChips;
        private System.Windows.Forms.Button btnReturnCoinBalance;
    }
}

